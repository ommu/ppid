<?php
/**
 * ppid module config
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 19 June 2019, 17:36 WIB
 * @link https://bitbucket.org/ommu/ppid
 *
 */

use ommu\ppid\Events;
use ommu\ppid\models\Ppid;

return [
	'id' => 'ppid',
	'class' => ommu\ppid\Module::className(),
	'events' => [
		[
			'class'    => Ppid::className(),
			'event'    => Ppid::EVENT_BEFORE_SAVE_PPIDS,
			'callback' => [Events::className(), 'onBeforeSavePpids']
		],
	],
];